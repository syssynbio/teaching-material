function demot25cells

%%% Cell population model (25 cells) + extracellular FGF4 
%%% For each cell: 4-variable Nanog/Gata6 model (DeMot) 
%%% FGF4: produced by every cell, local coupling.
%%% Heterogeneous cell population (variability Fgf4 perceived)
%%% For figures & movies: see plotresults.m
%%% Created: 28/3/2012 (Laurane De Mot & Didier Gonze)
%%% Updated: 11/10/2021 (Didier Gonze)

clear;
clc;

%%% Number of variables

NC=25;            % number of cells (perfect square, e.g. 25 or 36)
NV=5*NC;          % number of variables (5 var/cell, FGF incl)

%%% Initial conditions

xini1=[0 0 2.8 0.25 0.067];     % CI for one cell (G,N,FR,ERK,FGF4)
xini=[repmat(xini1',NC,1)'];    % CI for all cells

%%% Time parameters

trans=0;
tend=100;
tstep=0.1;

%%% Variability

varmax=3;    % variability max (in %, default 3)
np=1;         % number of parameters with variability

gamma=2*(varmax/100)*(rand(np,NC)-0.5);
%v=load('varia_01.dat');  

%%% Task:

integration(xini,trans,tend,tstep,gamma,NC);



%====================================================================
% Integration
%====================================================================

function output=integration(x0,trans,tend,tstep,v,NC);

% NC = number of cells
% v = vector of variability

%%% "Diffusion" matrix (matrix of first neighbours, periodic bounds) 

D=zeros(NC);
SNC=sqrt(NC);
M=reshape([1:NC],SNC,SNC);

for c=1:NC
[i,j]=find(M==c);
D(c,M(i,j))=1;
D(c,M(SNC-mod(SNC+1-i,SNC),j))=1;
D(c,M(i,SNC-mod(SNC+1-j,SNC)))=1;
D(c,M(i,mod(j,SNC)+1))=1;
D(c,M(mod(i,SNC)+1,j))=1;
end


%%% Simulation

[t,x] = run(x0,trans,tend,tstep,v,D);

%%% Results

Gend=x(end,1:5:end-1);
Nend=x(end,2:5:end);

nG=length(find(Gend>Nend & Nend<0.5));
nN=length(find(Nend>Gend & Gend<0.5));

fprintf('Number of cell ending in state G: %g\n',nG)
fprintf('Number of cell ending in state N: %g\n',nN)
fprintf('Number of cell ending in interm state: %g\n',NC-(nN+nG))


%%% Figures

figure(1)   % Times series for all cells

clf;
set(figure(1),'Position', [400 400 800 700]);  

k=0;

for i=1:SNC
for j=1:SNC
    
k=k+1;

if k<=NC
    
subplot(SNC,SNC,k)

%changecolororder()

plot(t,x(:,5*k-4),'b');  % G  for cell k
hold on;
plot(t,x(:,5*k-3),'r');  % N  for cell k
% hold on;
% plot(t,x(:,5*k-2),'g');  % FR  for cell k
% hold on;
% plot(t,x(:,5*k-1),'m');  % ERK  for cell k
% hold on;
% plot(t,x(:,5*k),'k');    % FGF4 (local)

if i==5
xlabel('Time','fontsize',12);
end
if j==1
ylabel('Concentration','fontsize',12);
end
xlim([0 tend]);
ylim([-0.1 3]);
set(gca,'xtick',[0:20:tend],'fontsize',8);
%legend('G','N','FR','ERK','FGF4');
%set(findobj(gca,'color','b'),'LineWidth',2);
%set(findobj(gca,'color','r'),'LineWidth',2);

end 

end
end



% figure(2)   % time series for the 1st cell
% 
% clf;
% 
% plot(t,x(:,1),'b');  % G  for cell k
% hold on;
% plot(t,x(:,2),'r');  % N  for cell k
% % hold on;
% % plot(t,x(:,3),'g');  % FR  for cell k
% % hold on;
% % plot(t,x(:,4),'m');   % ERK  for cell k
% % hold on;
% % plot(t,x(:,5),'k');  % FGF4 (extracell)
% 
% %legend('G','N','FR','ERK','FGF4');
% legend('G','N');
% xlabel('Time','fontsize',16)
% title('PrE','fontsize',20)
% ylim([-0.02 2.5])


figure(3)   % proportions

nI=NC-(nG+nN);    % number of undifferentiated cells (ICM)
eps=0.01;         % used to avoid problems in case n=0
nnn=[max(eps,nG) max(eps,nI) max(eps,nN)];

labels = {sprintf('PrE (%g)',nG),sprintf('ICM (%g)',nI),sprintf('Epi (%g)',nN)};

H=pie(nnn,labels);

set(H(1),'facecolor',[0.2 0.4 1],'edgecolor','w','linewidth',3);
set(H(3),'facecolor',[0.7 0.7 0.7],'edgecolor','w','linewidth',3);
set(H(5),'facecolor',[1 0.4 0.4],'edgecolor','w','linewidth',3);

set(H(2),'fontsize',16,'color','b');  % PrE (blue)
set(H(4),'fontsize',16,'color','k');  % ICM (grey) 
set(H(6),'fontsize',16,'color','r');  % Epi (red)


%%% Save data in file

R=[t x];

save results.dat R;
%save varia.dat v; 



%====================================================================
% Changecolororder
%====================================================================

function changecolororder

MyColors=[  0 0 1.0;   % blue
            1.0 0 0;   % red
            0 0.5 0;   % green
            1 0.5 0;   % orange
            0.75 0 0.75;
            0.75 0.75 0;
            0.25 0.25 0.25];
set(gca, 'ColorOrder', MyColors,'NextPlot','ReplaceChildren');


%====================================================================
% Run
%====================================================================

function [t,x]=run(x0,trans,tend,tstep,v,D)

ttrans = [0:tstep:trans];
tspan = [0:tstep:tend];

option = [];
%option = odeset('RelTol', 1e-5);                       % increases precision
%option=odeset('OutputS',[1:3],'OutputF','odeplot');    % display curves when computing

if trans > 0 
    [t x] = ode45(@dxdt,ttrans,x0,option,v,D);
    x0=x(end,:);
end

[t x] = ode45(@dxdt,tspan,x0,option,v,D);



%====================================================================
% dxdt
%====================================================================

function y = dxdt(t,x,zz,gamma,D)

%%% zz: not used, added to allow the script to run with Octave

%%% Number of cells

NC=(length(x(:,1)))/5;

%%% Parameters

vsg1=1.202;
Kag1=0.28;
vsg2=1;
Kag2=0.55;
Kig=2;
r=3;
s=4;
q=4;

kdg=1;

vsn1=0.856;
Kin1=0.28;
vsn2=1;
Kan=0.55;
Kin2=2;
u=3;
v=4;
w=4;

kdn=1;

vsfr1=2.8;
Kifr=0.5;
vsfr2=2.8;
Kafr=0.5;

kdfr=1;

va=20;
Kd=2;
Ka=0.7;
vi=3.3;
Ki=0.7;
ERKTOT=1;

vex=0;
vsf=0.6;
z=4;
Kaf=5;

kdf=0.0825;
%kdf=0.05;
%kdf=0.09; 
%kdf=0.11;


%%% variables

G=x(1:5:5*NC-4);        % Gata6
N=x(2:5:5*NC-3);        % Nanog
FR=x(3:5:5*NC-2);       % FGF4 receptor
ERK=x(4:5:5*NC-1);      % ERK
F=x(5:5:5*NC);          % FGF4

G=G';
N=N';
FR=FR';
ERK=ERK';
F=F';


%%% FGF perceived by each cell (each cell perceives F from itself and from the 4 neighbour cells

Fp=D*F';

%gamma=0.1*(rand(1,25)-0.5);

Fp=Fp'.*(1+gamma(1,:))/5;
%Fp=0.06;

%%% Equations

y(1:5:5*NC-4)=(vsg1*ERK.^r./(Kag1^r+ERK.^r)+vsg2*G.^s./(Kag2^s+G.^s)).*Kig.^q./(Kig^q+N.^q)-kdg*G;  % dG/dt
y(2:5:5*NC-3)=(vsn1*Kin1^u./(Kin1^u+ERK.^u)+vsn2*N.^v./(Kan^v+N.^v)).*Kin2^w./(Kin2^w+G.^w)-kdn*N; % dN/dt
y(3:5:5*NC-2)=vsfr1*(Kifr./(Kifr+N))+vsfr2*(G./(Kafr+G))-kdfr*FR;     % dFR/dt
y(4:5:5*NC-1)=va*FR.*(Fp./(Kd+Fp)).*((ERKTOT-ERK)./(Ka+ERKTOT-ERK))-vi*(ERK./(Ki+ERK));    % dERK/dt
y(5:5:5*NC)=vex+vsf*N.^z./(Kaf^z+N.^z)-kdf*F;      % dFGF4/dt


y=y';




