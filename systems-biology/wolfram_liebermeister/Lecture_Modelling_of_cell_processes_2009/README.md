Lecture "Modelling of cell processes"
-------------------------------------

* held by: W. Liebermeister
* held at: Humboldt University Berlin, Master "Theoretical Biophysics"
* held in: Winter semester 2009

Notes
* Some of the scripts are in German, some in English
* Lecture 3 was contributed by Jörg Schaber

Contact: W. Liebermeister