Talk  "A simple thermodynamic relation, very useful for metabolic modelling"
----------------------------------------------------------------------------

* held by: W. Liebermeister
* held at: Workshop "Principles of Microbial Adaptation", Lorentz Center Leiden
* held in: March 2018
