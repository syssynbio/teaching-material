Blackboard teaching on "Enzyme economy in metabolic models"
-----------------------------------------------------------

* held by: W. Liebermeister
* held at: [CompSysBio - Advanced Lecture Course on Computational Systems Biology](https://project.inria.fr/compsysbio2019/), Aussois 2019  
* held in: Spring 2019

------------------------------------------------------------
### Enzyme economy in metabolic models

Wolfram Liebermeister

INRA Jouy en Josas

Enzyme economy in metabolic models (Wolfram Liebermeister, INRAE Jouy en Josas)

In this blackboard course, you will learn about an economic aspect of microbial metabolism: the protein cost associated with metabolic fluxes. We discuss how this cost can be approximated based on a principle of minimal enzyme investments, how enzyme efficiencies can be used to predict metabolic fluxes, and how simplified cost functions for Flux Balance Analysis can be derived. By considering a partitioning of protein resources between ribosomes and metabolic enzymes, predictions about enzyme cost can be translated into cell growth rates. Then we have a brief look at large-scale constraint-based cell models that partition the protein budget into individual enzymes. Such models predict metabolic strategies and protein investments from metabolic network structure, from physical and physiological constraints (such as limited cell space, protein composition, and enzyme catalytic rates), and from an assumed drive for fast or efficient growth.

Prerequisites: basic understanding of cell biology, enzyme kinetics, and mathematical optimality problems.

Noor E., Flamholz A., Bar-Even A., Davidi D., Milo R., Liebermeister W. (2016). The protein cost of metabolic fluxes: prediction from enzymatic rate laws and cost minimization. PLoS Comput. Biol. 12: e1005167.
Wortel M.T., Noor E., Ferris M., Bruggeman F.J., Liebermeister W. (2018). Metabolic enzyme cost explains variable trade-offs between microbial growth rate and yield. PLoS Comput. Biol. 14: e1006010.
