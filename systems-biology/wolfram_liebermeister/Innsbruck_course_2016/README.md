PhD precourse session on cell models besides kinetic and flux analysis models
-----------------------------------------------------------------------------

* held by: W. Liebermeister
* held at: [Advanced lecture course on systems biology](https://sysbio-course.org/past-courses/sysbio2016/), Innsbruck 2016
* held in: Spring 2016

Notes
* The session was part of a precourse held together with E. Noor

Contact: W. Liebermeister