Systems biology and modeling block courses at HU Berlin, Theoretical Biophysics
-------------------------------------------------------------------------------

Scripts from the "Fachkurs" series for biophysics students (Theoretical Biophysics lab).

Courtesy of E. Klipp and many (then) members of the group who developed the course scripts.

The scripts in this folder originate from different courses in between 2004 and 2009 (SS: summer semester; WS: winter semester). For other scripts and exercises (e.g., introductions to python, matlab, R, Copasi, SBML), please contact W. Liebermeister.
