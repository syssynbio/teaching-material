\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage[top=1in, bottom=1in, left=1in, right=1in]{geometry}
\usepackage{lipsum}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{textgreek}
\usepackage[hidelinks]{hyperref}
\usepackage{graphicx}
\usepackage[
backend=biber,
style=numeric,
doi=false,
url=false,
maxbibnames=9
]{biblatex}
\addbibresource{compsysbio2021_blackboard.bib}

%\newcommand{\mymatrix}[1]{{\boldsymbol{#1}}}
\newcommand{\mymatrix}[1]{\textbf{#1}}
\newcommand{\myvector}[1]{{\boldsymbol{#1}}}
\newcommand{\fluxcone}{\mathcal{C}}
\newcommand{\stoichmat}{\mymatrix{S}}
\newcommand{\stoichmatext}{\mymatrix{S}_{\mathsf{ext}}}
\newcommand{\stoichmatint}{\mymatrix{S}_{\mathsf{int}}}

\date{November 15-16, 2021}
\title{\textbf{The laws of thermodynamics: applications in models of metabolism}\\CompSysBio 2021, Aussois, France}
\author{Elad Noor}
\begin{document}
\maketitle
\tableofcontents

\subsection*{Abstract}
We will start with the basic laws of thermodynamics, and see a few fundamental implications in enzyme catalysis and metabolism (including flux direction constraints, the Haldane relationship, and the flux-force relationship). We will also learn how to apply these laws to constraint-based metabolic models, and when selecting parameters for kinetic models. Finally, we will see how thermodynamics can also be used in optimization problems, such as the Max-min Driving Force (MDF) algorithm. Exercises will be carried out in Python. A good MILP solver (CPLEX or GUROBI) is recommended but not necessary.

\section{Introduction}

\subsection{The laws of thermodynamics}\label{sec:thermo-laws}

If you can read French, you might be interested in Sadi Carnot's original book about thermodynamics: \href{https://doi.org/10.24033/asens.88}{``Réflexions sur la puissance motrice du feu et sur les machines propres à développer atte puissance''} \cite{Carnot1824-gd}.

Over the years, thermodynamic theory developed further and is now typically stands upon 4 laws (or axioms):

\begin{itemize}
    \item \textbf{0$^\text{th}$ law} -- thermodynamic equilibrium is a transitive property.
    \item \textbf{1$^\text{st}$ law} -- energy is conserved.
    \item \textbf{2$^\text{nd}$ law} -- the entropy of an isolated system (such as the entire universe) can only increase over time.
    \item \textbf{3$^\text{rd}$ law} -- a perfect crysal structure at absolute zero will have no entropy.
\end{itemize}

In metabolic cell models, we assume that no work is done by the system and that it is isobaric and isothermal. Another fundamental principle in enzymatic catalysis is the notion that enzymes can accelerate the rate of a reaction, but not change its thermodynamic equilibrium, therefore they have no effect on the \textit{direction} of flux. Instead, reaction directions are entirely determined by the Gibbs free energy.

\subsection{The Gibbs free energy of reaction}\label{sec:gibbs}

The second law of thermodynamics states that entropy can only increase over time in an isolated system. In living cells, which are open systems in constant pressure, temperature, and pH, entropy can be replaced by the concept of transformed Gibbs free energy of reaction \cite{alberty_biochemical_2006-1}, denoted $\Delta_r G'$. Then, for a chemical reaction to be feasible, it must impose a negative change in $\Delta_r G'$:
\begin{eqnarray}\label{eq:thermo1}
    \Delta_r G' < 0
\end{eqnarray}

We already said that the presence of enzymes does not affect the $\Delta_r G'$ directly, so what does actually determine its value? We typically use the assumption that biochemical reactions occur in dilute aqueous solutions. In this case, the transformed Gibbs free energy of reaction $j$ is given by:
\begin{equation}
    \begin{split}\label{eq:thermo2}
    \Delta_r G' &= \Delta_r G'^\circ + R\,T\cdot\ln(Q') \\
    Q' &= \prod_i c_i^{\nu_i}
    \end{split}
\end{equation}
where $R$ is the gas constant, $T$ is the temperature (in Kalvin), $Q'$ is the biochemical reaction quotient, $c_i$ is the molar concentration of a reactant (substrate or product) and $\nu_i$ are the corresponding stoichiometric coefficients. $\Delta_r G'^\circ_j$ represents that $\Delta_r G'_j$ in standard conditions, which is typically defined as 1 M for each one of the reactants. One way to measure $\Delta_r G'^\circ_j$, is to let the reaction run until it reaches equilibrium, and measure the concentrations of all reactants. The value of $Q'$ in equilibrium is called the \textit{equilibrium constant} and is denoted $K_{eq}'$. Also, since we know that at equilibrium $\Delta_r G'_j = 0$, we can solve for $\Delta_r G'^\circ_j$ and get:
\begin{equation}\label{eq:equilibrium}
    \Delta_r G'^\circ = -R\,T\cdot\ln(K'_{eq}) = -R\,T\cdot \sum_i \nu_i \ln(c_i^{eq})
\end{equation}
where $c_i^{eq}$ are the measured molar concentraitons of the reactants at the equilibrium state.

You can find measured values for $K'_{eq}$ (based on equation \ref{eq:equilibrium}) on \href{http://dx.doi.org/10.18434/T40W22}{randr.nist.gov}. Unfortunately, only a small fraction of known biochemical reactions have been measured. \href{https://equilibrator.weizmann.ac.il/}{eQuilibrator} is an alternative that covers almost all enzyme-catalyzed reactions. The predictions are based on the component contribution method \cite{noor_consistent_2013}.

For one of the best introductions to thermodynamics of biochemical reactions, read the book by \textcite{alberty_biochemical_2006-1}.

\subsection{Enzyme kinetics and the connection to thermodynamics}
It is a well cited fact, that enzymes can catalyze the rate of a given reaction, but cannot alter the direction of net flux. In the following section (\ref{sec:flux-force}), we will see that an enzyme cannot even change the ratio between the forward and backward rates, only increase both of them proportionally.

But, is the converse true as well, or can information about the thermodynamics of a reaction tell us something about an enzyme catalyzing it? The answer is: yes, it can.

\textcite{Mellors1976-ry} explained how the relationship first described by JS Haldane in 1930 connects between an enzyme's kinetic parameters and the reaction's equilibrium constant. Imagine an enzyme that catalyzes a reaction $S \rightleftharpoons P$, whose flux is given by the following rate law:
\begin{equation}\label{eq:reversible_rate_law}
    v = E~\frac{k_\text{cat}^+ \cdot s / K_s - k_\text{cat}^- \cdot p / K_p}{1 + s / K_s + p / K_p}\,.
\end{equation}
where $E$ is the enzyme concentration, $k_\text{cat}^+$ and $k_\text{cat}^-$ are the forward and backward turnover numbers, $K_s$ and $K_p$ are the affinity constants, and $s$ and $p$ are the concentrations of the substrate and product. This rate law is sometimes mistakenly called reversible Michaelis-Menten kinetics, but is actually attributed to Haldane. He proved that given the standard assumptions about the catalytic energy profile, the following equation must be satisfied:
\begin{equation}
    K'_\text{eq} = \frac{k_\text{cat}^+ \cdot K_p}{k_\text{cat}^- \cdot K_s}\,.
\end{equation}
This equation is commonly known as the \textit{Haldane relationship}.

\subsection{The flux-force relationship}\label{sec:flux-force}

From the work of \textcite{beard_relationship_2007-1}, we know that there is a relationship between the thermodynamic driving force and the ratio of forward and backward
reaction rates:
\begin{eqnarray}
    \frac{v^+}{v^-} = e^{-\Delta_r G' / RT}
\end{eqnarray}
where the forward rate ($v^+$) and the backward rate ($v^-$) are the two opposing rates of a reversible reaction. The net flux is given by:
\begin{equation}
    v = v^+ - v^-
\end{equation}
When a reaction is close to equilibrium, e.g. when $\Delta_r G' > -10$ kJ/mol, the backward rate can become quite large and significantly reduce the net rate. At equilibrium ($\Delta_r G' = 0$), both forward and backward rates become equal and the net rate is zero.

The effect of the flux-force relationship can be explicitly quantified in a reformulation of the reversible Haldane rate law (equation \ref{eq:reversible_rate_law}), which we call the separable form \cite{noor_protein_2016}:

\begin{align}
    \begin{split}
        v &= E \cdot k_\text{cat}^+ \cdot \eta^\text{rev} \cdot \eta^\text{sat}\\
        \eta^\text{rev} &= 1 - e^{\Delta_r G' / RT}\\
        \eta^\text{sat} &= \frac{s/K_s}{1 + s/K_s + p/K_p}\,.
    \end{split}
\end{align}
$E$ is the enzyme concentration and $k_\text{cat}^+$ is the turnover rate (measured in the forward direction). The saturation term $\eta^\text{sat}$ is similar to an (irreversible) Michaelis-Menten rate law, except that there we usually ignore the product term ($p/K_p$) in the denominator. The reversibility term $\eta^\text{rev}$ is exactly the part that reflects how reversibility decreases the net flux. Both terms are always between 0 and 1.

For further reading on the flux-force relationship and how it can be used to restructure kinetic, see \cite{noor_note_2013}.



\subsection{Linear Programming (LP)}
Every Linear Programming problem (figure \ref{fig:linear_quadratic_programming}) can be converted to a canonical form that is:
\begin{eqnarray}
	\textsf{\textbf{Primal}}&&\nonumber\\
	&\textsf{maximize}&\myvector{c}^\top\myvector{x}\nonumber\\
	&\textsf{subject to}&\mymatrix{A}\myvector{x} \leq \myvector{b}\nonumber\\
	&\textsf{and}&\myvector{x}\geq \myvector{0}\,.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{eqnarray}

The dual is a symmetrical linear problem described by the following constraints
\begin{eqnarray}
	\textsf{\textbf{Dual}}&&\nonumber\\
	&\textsf{minimize}&\myvector{b}^\top\myvector{y}\nonumber\\
	&\textsf{subject to}&\mymatrix{A}^\top\myvector{y} \geq \myvector{c}\nonumber\\
	&\textsf{and}&\myvector{y}\geq 0\,.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{eqnarray}

If the primal has an optimal solution ($\myvector{x}^*$), then the dual will also have an optimal solution ($\myvector{y}^*$) and their values will be equal:
\begin{eqnarray}
	\textsf{\textbf{Strong duality theorem}}&&\nonumber\\
	&&\myvector{c}^\top\myvector{x}^* = \myvector{b}^\top\myvector{y}^*~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{eqnarray}

Linear Programming problems can be solved very efficiently using established algorithms such as Simplex. Many good solvers exist for LP, including open source options such as glpk, CLP, and the internal R-project solver.

\begin{figure}[h!]\label{fig:linear_quadratic_programming}
    \begin{center}
        \includegraphics[width=0.5\textwidth]{figures/linear_programming.pdf}
    \end{center}
    \caption{A graphical example for a problem with linear constraints, where the objective is either a linear function (Linear Programming) or a quadratic function (Quadratic Programming).}
\end{figure}

\subsection{Quadratic Programming (QP)}
Quadratic Programming is essentially the same as LP, except that the objective has an extra quadratic term:

\begin{eqnarray}
    \textsf{\textbf{QP}}&&\nonumber\\
    &\textsf{minimize}&\myvector{q}^\top\myvector{x} + \frac{1}{2}\myvector{x}^\top \mymatrix{Q}\myvector{x}\nonumber\\
    &\textsf{subject to}&\mymatrix{A}\myvector{x} \leq \myvector{b}\nonumber\\
    &\textsf{and}&\myvector{x}\geq \myvector{0}\,.~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{eqnarray}

It can essentially be solved using the same algorithms as LP and is therefore easily scalable. We recommend the  \href{https://www.cvxpy.org/}{\textsc{CVXPY}} package for Python users, but the Matlab function \href{https://www.mathworks.com/help/optim/ug/quadprog.html}{\textsc{quadprog}} is an alternative.


\subsection{Mixed-Integer Linear Programming (MILP)}
If an LP contains variables that can only have integer values (or in a more specific case, boolean values), the problem cannot be solved using the standard methods. In fact, MILP problems in general are NP-complete, i.e. computationally hard. The free solvers are typically not very efficient in solving large MILPs. Nevertheless, advanced commercial solvers such as \href{https://www.ibm.com/analytics/cplex-optimizer}{CPLEX} and \href{https://www.gurobi.com/}{Gurobi} are much more capable, and provide free licenses for academics.

\section{Convex analysis of a metabolic flux cone}
The basic idea underlying most metabolic constraint-based models, is mass-balance combined with the steady-state assumption. Formally, given a stoichiometric matrix $\mymatrix{S}$, where rows represent metabolites and column represent reactions, and given a flux vector $\myvector{v}$, the rate of change in metabolite levels ($\myvector{c}$) is given by the equation.
\begin{eqnarray}\label{eq:dynamic-mass-balance}
	&\textsf{\textbf{Dynamic mass-balance}} &\nonumber\\
    &&\frac{d\myvector{c}}{dt} = \stoichmat \myvector{v}~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{eqnarray}
All metabolites are separated into two groups, internal and external, where the boundary is typically defined by the cell outer membrane. However, mathematically, external metabolites are those that have such large pools (infinite) that we can essentially ignore their change over time even if it is not 0. These include things like atmospheric gases (O$_2$, CO$_2$) or the carbon and nitrogen sources in the medium surrounding the cells. According to this division, we can represent the stoichiometric matrix as a concatenation of two sub-matrices:
\begin{equation}\label{eq:stoichiometric_matrix}
    \stoichmat = \left[\begin{matrix}\stoichmatint \\ \stoichmatext\end{matrix}\right]
\end{equation}
For the subset of internal metabolites, the pseudo steady-state assumption states that their concentrations do not change over time.
\begin{eqnarray}\label{eq:pseudo-steady-state}
	&\textsf{\textbf{Pseudo steady-state}} &\nonumber\\
	&&\stoichmatint \myvector{v} = 0~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\end{eqnarray}
Therefore, pseudo steady-state flux solutions are contained within the null-space (or kernel) of $\stoichmatint$, i.e. $\myvector{v} \in \ker(\stoichmatint)$.

\subsection{The flux cone}
Typically, many of the reactions in our model will be considered irreversible, due to thermodynamic constraints (and we will elaborate on this topic in section \ref{sec:thermodynamics}). Therefore, a subset of the fluxes will be constrained to be positive:
\begin{eqnarray}\label{eq:irreversible}
\forall i \in I_\text{irr} ~~ v_i \geq 0
\end{eqnarray}

Therefore, the set of possible flux solutions would be all fluxes that satisfy both equation \ref{eq:pseudo-steady-state} and \ref{eq:irreversible}. Explicitly, it would be the intersection of all the hyperplanes that represent rows in $\stoichmatint$ described by $S_{j*}\cdot \myvector{v} = 0$ and all half-spaces corresponding to irreversible reactions $\myvector{e_i} \cdot \myvector{v} \geq 0$ (where $\myvector{e_i}$ is the unit vector corresponding to reaction $i$). This intersection is an unbound convex polyhedron denoted the steady-state \emph{flux cone} or $\fluxcone$ \cite{marashi_analysis_2012}.

It is important to note, that the flux cone is unbounded in some directions, i.e. it can extend to infinity. Therefore, we typically ignore the absolute values of the fluxes (represented, for example, by $||\myvector{v}||$) and consider only the relative values -- i.e. the direction towards which the vector is pointing. It is often easier to only consider normalized vectors, such as ones whose biomass rate is set to 1.

\begin{eqnarray}\label{eq:fluxcone}
	&\textsf{\textbf{Flux cone}} &\nonumber\\
	&&\fluxcone = \{\myvector{v} \in \mathbb{R}^n~|~\stoichmatint\myvector{v} = 0 ~\wedge ~ v_i \geq 0~~\forall i \in I_\text{irr}\}~~~~~~~~~~~~~~~~~
\end{eqnarray}

\subsection{Elementary Flux Modes}
Elementary Flux Modes (EFMs) are defined as the set of all non-decomposable vectors in the flux cone. A non-decomposable vector is one whose \textit{support} (the set of reactions with non-zero flux) is minimal -- i.e. the flux cone does not contain any vectors whose support is a proper subset of it. EFMs also form a convex basis for the flux cone \cite{schilling_theory_2000}:
\begin{eqnarray}
\forall \myvector{v} \in \fluxcone:~ \exists \lambda_j \geq 0 ~~\text{s.t.}~~ \myvector{v} = \sum_j \lambda_j~\myvector{e}^j
\end{eqnarray}
where $\{\myvector{e}^j\}_j$ is the set of all EFMs. Figure \ref{fig:toy1} illustrates a model with 10 reactions and 9 EFMs.
\begin{figure}[ht!]
	\begin{center}
	\includegraphics[width=0.8\textwidth]{figures/toy1}
	\caption{A toy example with 6 internal reactions and 4 exchange reactions. The flux cone is spanned by 9 EFMs.}\label{fig:toy1}
	\end{center}
\end{figure}

EFMs are closely related to Extreme Pathways (EPs) and distinguishing them requires more subtle definitions. For further reading you can refer to \cite{klamt_two_2003}.

\subsection{Classification of EFMs}
Even in our small toy example in Figure \ref{fig:toy1}, we already encounter a common problem that arises when dealing with EFMs -- i.e. cycles. In this example, it's quite obvious that the two EFMs that consist of combining two opposing reactions is futile and should be ignored. Larger networks, however, might have much larger cycles that are harder to identify and get rid of.

First, we need to distinguish between \textit{primary exchange} and \textit{currency exchange} reactions. The former are the standard reactions that exchange nutrients between the cell/compartment and its environment (sugar import, CO$_2$ export, etc.). \textit{Currency exchange} reactions do not, in fact, exchange material between compartments, but are rather abstract representations of energy dissipation. A canonical example would be the ATP maintenance reaction that is present in most metabolic models: ATP + H$_2$O $\rightharpoonup$ ADP + P$_i$.

Now, we can define the three types of EFMs (\cite{price_extreme_2002}, see Figure \ref{fig:efm_types}):
\begin{description}
	\item[I] -- \textit{Primary systemic EFMs} have at least one active primary exchange flux.
	\item[II] -- \textit{Futile cycles} have no active primary exchange fluxes, and at least one active currency exchange flux.
	\item[III] -- \textit{Internal cycles} have no active exchange fluxes (neither primary nor currency).
\end{description}

\begin{figure}[ht!]
	\begin{center}	\includegraphics[width=0.7\textwidth]{figures/extreme_pathway_types}
	\caption{The three types of Elementary Flux Modes.}\label{fig:efm_types}
	\end{center}
\end{figure}

To illustrate the three types of pathways, we present another toy model with one currency exchange reaction and two primary exchange reactions (Figure \ref{fig:toy2}). This model contains all the three types of EFMs.
\begin{figure}[ht!]
	\begin{center}
	\includegraphics[width=0.5\textwidth]{figures/toy2}
	\caption{A small toy model with 8 internal reactions, 2 primary exchange reactions, and one currency exchange reaction. In this simple network, one can identify all three types of extreme pathways. The Type I pathway (green) is typically the type of solution that most constrain-based models are seeking. The Type II pathway ($A \rightarrow B \rightarrow C \rightarrow A$, blue) is a typical futile cycle, since it does not involve any primary exchange reactions, but does \emph{waste} ATP. The Type III pathway ($A \rightarrow C \rightarrow D \rightarrow A$, red) is called internal since none of its reactions are exchange reactions. An internal cycle will never be thermodynamically feasible.
	}\label{fig:toy2}
	\end{center}
\end{figure}

In the next section, we will see how this three type classification is useful to separate thermodynamically feasible pathways from infeasible ones.

\section{Thermodynamics in constraint-based models}\label{sec:thermodynamics}

We would like to rewrite equation \ref{eq:thermo1} in a more convenient way that corresponds well with our linear algebra notation. First, one can notice that taking the log from $Q'$ makes it a linear function of the log-concentration values: $\ln(Q'_j) = \sum_i S_{ij} \cdot \ln(c_i)$. In addition, from now on we will use a matrix notation for calculating the vector of all reaction Gibbs energies ($\myvector{\Delta_r G'}$):
\begin{eqnarray}
    \myvector{\Delta_r G'} = \myvector{\Delta_r G'^\circ} + R\,T\cdot\stoichmat^\top \myvector{x}
\end{eqnarray}
where we define $\myvector{x}$ as the vector of log-concentrations, i.e. $x_i = \ln(c_i)$.



\subsection{Irreversibility is an approximation}\label{sec:irreversibility}
In pure physics terms, every chemical reaction should be reversible. There are cases, where the equilibrium is so far from unity, which makes it impossible for the reaction to reach equilibrium in practice. In these cases, it is sometimes convenient to assume that the reaction is irreversible and complete ignore the reverse direction. In defining the flux cone, this assumption can significantly reduce the solution space and number of EFMs.

Most thermodynamics-aware models, however, cannot make this assumption. Every reaction must have a defined equilibrium constant, no matter how extreme it might be. Therefore, from now on, we assume $\Delta_r G'^\circ_j$ has a finite value, even for reactions that are defined as irreversible in our model. There is no universal threshold for $\Delta_r G'^\circ_j$ which make a reaction irreversible. It is context dependent and a topic of much debate \cite{mavrovouniotis_identification_1993-1, henry_thermodynamics-based_2007, noor_integrated_2012}.

Beyond $\Delta_r G'^\circ_j$, probably the most important parameters for determining the reversibility are the range of concentrations allowed for each of the reactants. Basically, a reversible reaction should have both positive and negative values to its $\Delta_r G'_j$, depending on the chosen reactant concentrations within their predefined ranges. If we denote the vectors of lower and upper bounds as $\myvector{b}^L$ and $\myvector{b}^U$, respectively, the constraint on $\myvector{x}$ would be:
\begin{eqnarray}
\ln(\myvector{b}^L) \leq \myvector{x} \leq \ln(\myvector{b}^U)
\end{eqnarray}
It is easy to see, that the lowest value for $\Delta_r G'_j$ is achieved when all substrate concentrations are set to their value in $\myvector{b}^U$, and all product concentrations to $\myvector{b}^L$. The highest value of $\Delta_r G'_j$ occurs in the opposite extreme. Then, it become straightforward to check if a reaction is, by itself, reversible or not.

\subsection{Thermodynamic Feasibility of EFMs}\label{sec:feasibility}
Given a specific elementary flux mode $\myvector{e}^j$ (or any flux vector $\myvector{v}$, for that matter), we would like to know whether it is thermodynamically feasible according to the second law. First, every one of the support reactions must be feasible in the direction defined by $\myvector{e}^j$. However, the same metabolite can be a substrate for one reaction, and a product for another and therefore there is inter-dependence between the $\Delta_r G'_j$ values of these reactions. Sometimes, even though a series of reactions can be individually feasible, their combination is infeasible. This has been termed a \textit{distributed bottleneck} by \textcite{mavrovouniotis_identification_1993-1} in 1993.

Using Linear Programming, it is relatively simple to test thermodynamic feasibility:
\begin{align}
    \begin{split}\label{eq:feasible_lp}
        \textsf{\textbf{Feasbility}} ~~~~~~~\\
        \exists&\myvector{x}\\
        \textsf{such that}~~~~~~~&\\
        \myvector{\Delta_r G'} &\equiv \myvector{\Delta_r G'^\circ} + RT \cdot \stoichmat^\top \myvector{x}\\
        \myvector{\Delta_r G'} &< 0\\
        \ln(\myvector{b}^L) &\leq \myvector{x} ~\leq~ \ln(\myvector{b}^U)
    \end{split}
\end{align}

\subsection{Max-min Driving Force}\label{sec:mdf}
In order to provide a more quantitative measure for the thermodynamic feasibility of a given pathway, the Max-min Driving Force (MDF) method provides a relatively simple measure. We adjust the Linear Program in Equation \ref{eq:feasible_lp}, by adding a margin variable ($B$) and maximizing its value:
\begin{align}
    \begin{split}
        \textsf{\textbf{MDF}} ~~~~~~~\\
        B^* &= \max_{B,\myvector{x}}{B} \\
        \textsf{such that}~~~~~~~&\\
        \myvector{\Delta_r G'} &\equiv \myvector{\Delta_r G'^\circ} + RT \cdot \stoichmat^\top \myvector{x} \\
        \myvector{\Delta_r G'} &< -B\\
        \ln(\myvector{b}^L) &\leq \myvector{x} ~\leq~ \ln(\myvector{b}^U)
    \end{split}
\end{align}
If the MDF is positive, then there exists a set of concentrations $\myvector{x}$ (inside the allowed range) such that $\myvector{\Delta_r G'} \leq -B < 0$, i.e. this pathway is thermodynamically feasible. The larger $B$ is, the ``more feasible'' it is, since we can keep all reactions farther away from equilibrium. According to the flux-force relationship (section \ref{sec:flux-force}), the farther one is from equilibrium, the lower the backward flux is relative to the forward and therefore the reaction is more efficient. An example for MDF calculation is given in Figure \ref{fig:mdf}.

An online interface for running MDF analysis can be found here: \url{https://equilibrator.weizmann.ac.il/pathway/}.

\begin{figure}[ht!]
	\begin{center}
		\includegraphics[width=0.8\textwidth]{figures/mdf}
		\caption{(left) Schematic comparison between two pathways. Each pathway starts and ends with the same compounds, employs five enzymes and carries the same net flux. The kinetic parameters of all enzymes in both pathways, as well as enzyme and metabolite concentrations, are assumed to be identical. (right) Energetic profile of Embden-Meyerhof-Parnas glycolysis. Dashed black line corresponds to $\Delta_r G'^\circ$ values (metabolite concentrations of 1M) of pathway reactions at pH 7.5. Red line corresponds to $\Delta_r G'$ values of pathway reactions after an optimization procedure that maximizes the driving force of the thermodynamic bottleneck reactions. Figure is from \textcite{noor_pathway_2014}.
		}\label{fig:mdf}
	\end{center}
\end{figure}

\subsection{Thermodynamic Flux Balance Analysis (TFBA)}\label{sec:tmfa}

Thermodynamic FBA (also known as Thermo\-dynamic-based Metabolic Flux Analysis \cite{henry_thermodynamics-based_2007}) was designed to deal with thermodynamically infeasible flux solutions within the framework of FBA:
\begin{align}
    \begin{split}\label{eq:fba}
        \textsf{\textbf{FBA}}~~~~~~~& \\
        \myvector{v^*} &= \mathrm{arg\max_v} {~\myvector{c}^\top\myvector{v}}\\
        \textsf{such that}~~~~~~~~& \\
        \stoichmatint \myvector{v} &= \myvector{0}\\
        \myvector{v}_{LB} &\leq \myvector{v} ~\leq~ \myvector{v}_{UB}
\end{split}
\end{align}
where $\myvector{c} \in \mathbb{R}^r$ is the objective function, and the constants are the internal stoichiometric matrix $\stoichmatint \in \mathbb{R}^{m \times r}$. $\myvector{v}_{LB}$ and $\myvector{v}_{UB}$ are the lower and upper bounds on the fluxes, typically relevant only for exchange fluxes.

TFBA adds another two sets of variables -- the boolean flux indicators ($\myvector{y} \in \{0, 1\}^r$), and the log-concentrations ($\myvector{x} \in \mathbb{R}^r$). Then, the following constraints are added to the Linear Problem (which is now actually a Mixed-Integer Linear Problem -- MILP):
\begin{eqnarray}
\textsf{\textbf{TFBA}}~~~~~~~&& \nonumber\\
\myvector{v^*} &=& \mathrm{arg\max_v} {~\myvector{c}^\top\myvector{v}}\nonumber\\
\textsf{such that}~~~~~~~&& \nonumber\\
\stoichmatint \myvector{v} &=& \myvector{0} \nonumber\\
\myvector{v}_{LB} &\leq& \myvector{v} ~\leq~ \myvector{v}_{UB} \nonumber\\
0 &\leq& M \myvector{y} - \myvector{v} ~\leq~ M  \label{eq:tfba1}\\
0 &<& M \myvector{y} + \myvector{\Delta_r G'} ~<~ M \label{eq:tfba2} \\
\myvector{\Delta_r G'} &=& \myvector{\Delta_r G'^\circ} + RT \cdot \stoichmat^\top \myvector{x} \label{eq:tfba3}\\
\ln(\myvector{b}^L) &\leq& \myvector{x} ~\leq~ \ln(\myvector{b}^U) \label{eq:tfba4}
\end{eqnarray}
The new constants are the vector of standard Gibbs energies of reaction $\myvector{\Delta_r G'^\circ}$ (in units of kJ/mol), the gas constant $R$ = 8.31 J/mol/K and temperature $T$ = 300 K. $M$ which is a very large number (larger than any of the possible flux and Gibbs free energy). Note also the difference between the internal stoichiometric matrix ($\stoichmatint$) used for the pseudo steady-state assumption, versus the full stoichiometric matrix ($\stoichmat$) used in the Gibbs free energy calculation.

Equations \ref{eq:tfba3}-\ref{eq:tfba4} should be familiar, and are exactly the same as in previous sections. To understand how the other two constraints (\ref{eq:tfba1}-\ref{eq:tfba2}) enforce thermodynamic feasibility, we consider three possible cases for each reaction $j$ separately:
\begin{enumerate}
	\item $v_j > 0$ : the only possible value that $y_j$ can have is 1, otherwise, $My_j - v_j$ would be negative and violate constraint \ref{eq:tfba1}. Therefore, constraint \ref{eq:tfba2} becomes $0 < M + \Delta_r G'_j < M$, which means that $\Delta_r G'_j < 0$.
	\item $v_j < 0$ : the only possible value that $y_j$ can have is 0, otherwise, $My_j - v_j$ would be larger than $M$ and violate constraint \ref{eq:tfba1}. Therefore, constraint \ref{eq:tfba2} becomes $0 < \Delta_r G'_j < M$, which means that $\Delta_r G'_j > 0$.
	\item $v_j = 0$ : both $0$ and $1$ are possible solutions for $y_j$. Therefore, there are no constraints on $\Delta_r G'_j$.
\end{enumerate}
Summarizing these 3 cases, one can concisely write:
\begin{eqnarray}
\forall j:v_j = 0~\vee~\text{sign}(v_j) = -\text{sign}(\Delta_r G'_j)\,,
\end{eqnarray}
which is exactly the second law of thermodynamics.

\subsection{Loopless Flux Balance Analysis (ll-FBA)}\label{sec:ll-fba}
The loopless algorithm \cite{schellenberger_elimination_2011} is very similar to TFBA, except that there are no actual thermodynamic values. This way, thermodynamically infeasible internal (Type III) cycles are eliminated, while all other pathways are kept \cite{noor_proof_2012}. The set of equations describing ll-FBA are:
\begin{eqnarray}
\textsf{\textbf{ll-FBA}}~~~~~~~&& \nonumber\\
\myvector{v^*} &=& \mathrm{arg\max_v} {~\myvector{c}^\top\myvector{v}}\nonumber\\
\textsf{such that}~~~~~~&& \nonumber\\
\stoichmatint \myvector{v} &=& \myvector{0} \label{eq:llfba1} \\
\myvector{v}_{LB} &\leq& \myvector{v} ~\leq~ \myvector{v}_{UB} \\
0 &\leq& M \myvector{y} - \myvector{v} ~\leq~ M
\label{eq:llfba2} \\
0 &<& M \myvector{y} + \myvector{\Delta_r G'} ~<~ M \label{eq:llfba3} \\
\myvector{\Delta_r G'} &\in& \ker{(\stoichmat)}^\perp \label{eq:llfba5}
\end{eqnarray}
here, $\myvector{\Delta_r G'}$ is not constrained by the $\myvector{\Delta_r G'}^\circ$ and the metabolite concentrations, but is only required to be orthogonal to the null-space of $\stoichmat$ (or, equivalently, to be in $\mathrm{image}(\stoichmat^\top)$).

Why does ll-FBA not eliminate type II cycles from the set of flux solutions? These futile cycles ``waste'' resources such as ATP, but are thermodynamically feasible. They are only considered ``cycles'' because we chose to give co-factors a special status (namely, they are external metabolites that do not need to be kept at steady-state). In other words, type II EFMs are in the null-space of $\stoichmatint$, but not in the null-space of $\stoichmat$ (just like type I EFMs). Therefore, it is possible to assign negative $\Delta_r G'$ values to all reactions in a type II EFMs.

Interestingly, this argument can be applied also to the reverse of such futile cycle. Running a futile cycle in reverse is sometimes called an Energy Generating Cycle (EGC), and is obviously unrealistic. Furthermore, unlike type III cycles that do not affect the biomass rate in standard FBA, EGCs have the potential to increase the maximal biomass yield and pose a more serious problem in FBA models \cite{fritzemeier_erroneous_2017-2}. This is one of the cases where TFBA differs from ll-FBA, as it prevents the use of EGCs completely, while ll-FBA doesn't.

\section{Gibbs energy dissipation}

\subsection{The Gibbs energy dissipation rate of a reaction}\label{sec:dissipation_single}
For a single reaction, the Gibbs energy dissipation rate of a reaction $i$ is simply the negation of its $\Delta_r G'$ multiplied by the net flux:

\begin{equation}
    g_i = -\Delta_r G'_i \cdot v_i
\end{equation}

Note that if $v_i \neq 0$, then the second law of thermodynamics states that $\Delta_r G'_i$ and $v_i$ must have opposite signs -- therefore $g_i \geq 0$ is always true.

\subsection{The Total Gibbs energy dissipation rate}\label{sec:dissipation_total}
The total Gibbs energy dissipation rate of an entire cell is defined as $g^\text{diss} \equiv \sum_i g_i$. If we represent fluxes in units per gram of dry biomass (e.g. \textmu mol/min/gr), then $g^\text{diss}$ can be converted to units of Watts per gram (J/s/gr). \textcite{niebel_upper_2019} hypothesized that the total dissipation rate per unit of biomass might be capped, and showed how adding this constraint to an existing metabolic model can help predictions. To calculate $g^\text{diss}$, they started with a standard thermodynamic model (such as in TFBA -- see section \ref{sec:tmfa}) and added the extra constraints needed for the upper bound on total dissipation:

\begin{align}
    \begin{split}\label{eq:tfba_with_gdiss}
    \textsf{\textbf{TFBA}}^\dagger ~~~~~~~& \\
    \myvector{v^*} &= \mathrm{arg\max_v} {~\myvector{c}^\top\myvector{v}}\\
    \textsf{such that} ~~~~~~~& \\
    \stoichmatint \myvector{v} &= \myvector{0} \\
    \myvector{v}_{LB} &\leq \myvector{v} ~\leq~ \myvector{v}_{UB} \\
    0 &\leq M \myvector{y} - \myvector{v} ~\leq~ M \\
    0 &< M \myvector{y} + \myvector{\Delta_r G'} ~<~ M \\
    \myvector{\Delta_r G'} &= \myvector{\Delta_r G'^\circ} + RT \cdot \stoichmat^\top \myvector{x}\\
    \ln(\myvector{b}^L) &\leq \myvector{x} ~\leq~ \ln(\myvector{b}^U)\\
    g^\text{diss} &= -\myvector{\Delta_r G'}^\top \myvector{v} \\
    g^\text{diss} &\leq g^\text{diss}_\text{max}
    \end{split}
\end{align}
$^\dagger$ -- with an upper bound on Gibbs energy dissipation rate.

Sometimes, it might be useful to calculate $g^\text{diss}$ without committing to a full TFBA model (which requires collecting all reaction energies and solving it using an MILP solver). Here, we'll see how the system in \ref{eq:tfba_with_gdiss} can be converted to a simple LP if we are fine with ignoring the second law constraints of internal reactions ($\Delta_r G' \leq 0$).

First, we recall that any set of reaction Gibbs energies can be represented by formation energies multiplied by the stoichiometric matrix:

\begin{equation}
    \myvector{\Delta_r G'} = \stoichmat^\top \cdot \myvector{\Delta_f G'}\,.
\end{equation}
Then, the total dissipation rate can be rewritten as:
\begin{equation}
    g^\text{diss} = -\myvector{\Delta_r G'}^\top \myvector{v} = -(\stoichmat^\top \cdot \myvector{\Delta_f G'})^\top \myvector{v} = -\myvector{\Delta_f G'}^\top\stoichmat~\myvector{v}\,.
\end{equation}
The full stoichiometric matrix is a concatenation of the external ($\stoichmatext$) and internal matrix ($\stoichmatint$), so if we consider the steady-state assumption ($\stoichmatint \myvector{v} = \myvector{0}$), we can continue to simply the expression such that:
\begin{equation}
    g^\text{diss} =
    -\myvector{\Delta_f G'}^\top \left[\begin{matrix}\stoichmatint \\ \stoichmatext \end{matrix}\right] \myvector{v} =
    -\myvector{\Delta_f G'}^\top \left[\begin{matrix}\stoichmatint \myvector{v}\\ \stoichmatext \myvector{v} \end{matrix}\right] =
    -\myvector{\Delta_f G'}^\top \left[\begin{matrix}\myvector{0}\\ \stoichmatext \myvector{v} \end{matrix}\right] =
    -\myvector{\Delta_f G'}_\mathsf{ext}^\top~\stoichmatext~\myvector{v}\,,
\end{equation}
where $\myvector{\Delta_f G'}_\mathsf{ext}$ is the vector of formation energies for the external metabolites (a sub-vector of $\myvector{\Delta_f G'}$). Since the concentration of external metabolites is fixed by definition, $\myvector{\Delta_f G'}_\mathsf{ext}$ will have fixed values that can usually be computed directly. Using this result, we can formulate the standard FBA problem with an upper bound on the total Gibbs energy dissipation rate:
\begin{align}
    \begin{split}\label{eq:fba_with_gdiss}
        \textsf{\textbf{FBA}}~~~~~~~& \\
        \myvector{v^*} &= \mathrm{arg\max_v} {~\myvector{c}^\top\myvector{v}}\\
        \textsf{such that}~~~~~~~~& \\
        \stoichmatint \myvector{v} &= \myvector{0}\\
        \myvector{v}_{LB} &\leq \myvector{v} ~\leq~ \myvector{v}_{UB} \\
        g^\text{diss}_\text{max} &\geq -\myvector{\Delta_f G'}_\mathsf{ext}^\top~\stoichmatext~\myvector{v}
    \end{split}
\end{align}

\printbibliography

\end{document}

