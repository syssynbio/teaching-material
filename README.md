# Teaching Materials for Systems Biology

We created this project to collect teaching material for researchers and students interested in Systems Biology. In our GitLab repository, you can find lectures, transcripts, slides, tutorials and other material that might help your teaching. You can find all the collected files in the folder [systems-biology](./systems-biology). An overview of materials can be found below.

## How you can contribute

We invite everyone to contribute their material to this repository. All you need is to follow a few simple rules:
- We require that the files you upload are shared under a [Creative Commons](https://creativecommons.org/) license. Please specify one of the four open licenses ("CC BY" - our recommendation, "CC BY SA", "CC BY NC", or "CC BY NC SA"). Please read what that means if you are not familiar with it. 
- Then, make sure that you own the copyright, or that you have permission to share the art from its owners. We have all been there, when using an image or graphic that we found through an online search. These are usually protected by copyright, and must be removed from your slides before uploading them.
- Ideally, create a Pull Request with the new files on this repository. Don't forget to add an extra file with your name, affiliation, and the date (and any other information you feel is relevant) so we can keep track of who contributed what.
- If you don't feel comfortable with Git, you can also just email your files to us directly, and we'll take care of it.

That's it! We hope that this repository will grow quickly and be used by as many people as possible. Of course, any suggestions on how to improve it are very welcome - you can use the [Issues](https://gitlab.com/ecobiol/teaching-material/-/issues) tab for that, or our dedicated [Gitter](https://gitter.im/ecobiol/community) channel. Also, if you'd like to join us as a maintainer of the project, let us know. We will be delighted.

## Collected teaching materials

### Cell biology

Book "Cell biology by the numbers" by Ron Milo and Rob Phillips, related to the [BioNumbers](https://bionumbers.hms.harvard.edu/) database.
A draft version of the book can be freely downloaded from [book.bionumbers.org](http://book.bionumbers.org/).
We recommend watching the lectures by Ron Milo: 
* TEDX talk ["A sixth sense for understanding our cells"](https://www.youtube.com/watch?v=JC7WnzM2Lsc) 
* Full BioNumbers lectures on [youtube](https://www.youtube.com/channel/UChCzuzoZp5NheAPWH5YoGWw)

### Systems biology

* [Lecture slides "Introduction to systems biology"](./systems-biology/cri-systems-biology/2020/SysBio_2020_Lecture_1_Introduction_to_systems_biology.pdf) (W. Liebermeister, 2020) 
* [Lecture slides "High-throughput experiments - The omics revolution in Systems Biology"](./systems-biology/cri-systems-biology/2022/SysBio_2022_Lecture_2_Jules_High-throughput-experiments.pdf) (M. Jules, 2022) 
* [Lecture slides "From data to models in molecular biology (part 1)"](./systems-biology/cri-systems-biology/2022/SysBio_2022_Lecture_5_Calzone_Martignetti_From_data_to_models_1.pdf) (L. Calzone and L. Martignetti, 2022) 
* [Lecture slides "From data to models in molecular biology (part 2)"](./systems-biology/cri-systems-biology/2022/SysBio_2022_Lecture_5_Calzone_Martignetti_From_data_to_models_2.pdf) (L. Calzone and L. Martignetti, 2022) 

### Cellular networks

* [Course script on cellular networks](./systems-biology/hu-berlin-fachkurse/Cellular_Networks_WS2005.pdf) (HU Berlin Fachkurs, 2005, 2005)
* [Lecture slides "Biochemical network models"](./systems-biology/cri-systems-biology/2020/SysBio_2020_Lecture_2_Biochemical_network_models.pdf)  (W. Liebermeister, 2020)
* [Blackboard session on qualitative dynamical modeling](./systems-biology/denis_thieffry/CompSysBio2021) (D. Thieffry) 
* [Lecture slides "Metabolic networks"](./systems-biology/cri-systems-biology/2020/SysBio_2020_Lecture_3a_Metabolic_networks.pdf) (W. Liebermeister, 2020)
* [Lecture script "Network motifs"](./systems-biology/wolfram_liebermeister/Lecture_Modelling_of_cell_processes_2009/course_script/script_4_network_motifs.pdf) (W. Liebermeister, 2009) 
* [Lecture slides "Network motifs"](./systems-biology/cri-systems-biology/2020/SysBio_2020_Lecture_3b_Network_motifs.pdf) (W. Liebermeister, 2020)

### Regulation of gene expression

* [Lecture slides "Gene expression regulation and noise"](./systems-biology/cri-systems-biology/2022/SysBio_2022_Lecture_3_Jules_Gene-expression-regulation-and-noise.pdf) (M. Jules, 2022) 

### Metabolic models

* [Course script on enzyme kinetics and MCA (with exercises)](./systems-biology/hu-berlin-fachkurse/Enzyme_Kinetics_MCA_WS2009.pdf) (HU Berlin Fachkurs, 2009, 2009)
* [Lecture script "Kinetic models"](./systems-biology/wolfram_liebermeister/Lecture_Modelling_of_cell_processes_2009/course_script/script_2_kinetic_models.pdf) (W. Liebermeister, 2009) 
* [Blackboard session SysBio 2016 1](./systems-biology/wolfram_liebermeister/Innsbruck_course_2016) (W. Liebermeister)
* [Blackboard session SysBio 2018 1](./systems-biology/wolfram_liebermeister/Innsbruck_course_2018)/[2](./systems-biology/elad_noor/Innsbruck_Course_2018),  (E. Noor, W. Liebermeister)
* [Blackboard session on integrated networks of metabolism and gene expression](./systems-biology/hidde_de_jong/CompSysBio2021) (H. de Jong, 2021) 

### Thermodynamics and bioenergetics

* [Lecture "Cell bioenergetics"](./systems-biology/oliver_ebenhoeh/20230202-Lecture_Script_QBio301-2.pdf) (O. Ebenhoeh)
* [Presentation "Introduction to thermodynamics"](./systems-biology/oliver_ebenhoeh/thermodynamicsIntroduction.odp) (O. Ebenhoeh)
* [Blackboard session "The laws of thermodynamics: applications in models of metabolism"](./systems-biology/elad_noor/CompSysBio2021) (E. Noor) 
* [Talk "A simple thermodynamic relation"](./systems-biology/wolfram_liebermeister/Lorentz_Center_2018) (W. Liebermeister) 

### Resource allocation and enzyme economics

* [Lecture "Whole-cell models"](./systems-biology/wolfram_liebermeister/PSL-ITI_Whole_Cell_Modeling_2017) (W. Liebermeister, 2017) 
* [Online course "Resource allocation models"](./systems-biology/wolfram_liebermeister/Lab_course_Milo_group_2020) (W. Liebermeister) 
* [Lecture slides "Resource allocation and growth"](./systems-biology/cri-systems-biology/2021/SysBio_2021_Lecture_5_Bertaux_Resource_Allocation.pdf) (F. Bertaux, 2021)
* [Blackboard session "Enzyme economy in metabolic models" 2021](./systems-biology/wolfram_liebermeister/CompSysBio2021) (W. Liebermeister)

### Economy of the cell

Lectures

* [Cellular economics](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-1-INTRO_Introduction.pdf) (W. Liebermeister)
* [What makes up a cell?](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-2-CEN_What_makes_up_a_cell.pdf) (D. Szeliova)
* [Optimality in cells](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-3-OPT_Optimality.pdf) (M. Köbis)
* [A dynamic view of metabolism](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-4-MET_Dynamic_metabolism.pdf) (O. Soyer)
* [Flux balance analysis](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-6-FLX_FBA.pdf) (S. Waldherr)
* [Cost of metabolic pathways](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-7-PAT_Cost_of_metabolic_pathways.pdf) (E. Noor)
* [Optimal metabolic states](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-8-OME_Optimal_metabolic_states.pdf) (M. Wortel)
* [Self-replicator cell models](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-9-SMA_Self_replicator_cell_models.pdf)
  [(Notes)](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-9-SMA_Self_replicator_cell_models-NOTES.pdf)  (O. Golan)
* [Resource allocation models](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-10-LAR_RBA.pdf) (A. Goelzer)
* [Optimal cell behavior in time](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Lecture-11-DYN_Optimal_behavior_in_time.pdf) (H. de Jong)

Exercises

* [Cell components](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_CEN_What_makes_up_a_cell.pdf) (D. Szeliova)
* [Balanced growth](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_BAL_Balanced_cell_growth.pdf) (M. Wortel)
* [FBA (jupyter notebook)](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_FLX_Flux_balance_analysis.ipynb) (S. Waldherr)
* [Pathway fluxes and cost](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_PAT_Cost_of_metabolic_pathways.pdf) (E. Noor)
* [Optimal metabolic states](./systems-biology/epcp-summer-school-2022/EPCP_Paris_2022_Exercise_OME_Optimal_metabolic_states.pdf) (M. Wortel)

### Cell interactions, population dynamics, and evolution

* [Lecture slides "Compromise, competition and cooperation"](./systems-biology/cri-systems-biology/2020/SysBio_2020_Lecture_8_Compromise_competition_and_cooperation.pdf) (W. Liebermeister, 2020)
* [Lecture slides "Population dynamics and evolution"](./systems-biology/cri-systems-biology/2021/SysBio_2021_Lecture_6_Bertaux_Population_Dynamics_Evolution.pdf) (F. Bertaux, 2021) 

### Mathematical modeling and bioinformatics

* [Course script on dynamical systems](./systems-biology/hu-berlin-fachkurse/Dynamical_Systems_WS2009.pdf) (HU Berlin Fachkurs, 2009)
* [Lecture script "Modeling formalisms besides kinetic models" (in German)](./systems-biology/wolfram_liebermeister/Lecture_Modelling_of_cell_processes_2009/course_script/script_3_other_formalisms_german.pdf) (W. Liebermeister, 2009) 
* [Blackboard session on multistability, bifurcation, and differentiation](./systems-biology/didier_gonze/CompSysBio2021) (D. Gonze) 
* [Course script on parameter estimation and model selection](./systems-biology/hu-berlin-fachkurse/Parameter_Estimation_Model_discrimination_WS2009.pdf) (HU Berlin Fachkurs, 2009)
* [Lecture script "Model reduction and coupling"](./systems-biology/wolfram_liebermeister/Lecture_Modelling_of_cell_processes_2009/course_script/script_6_model_reduction_and_coupling.pdf) (W. Liebermeister, 2009) 
* [Course script on stochastic processes](./systems-biology/hu-berlin-fachkurse/Stochastic_processes_SS2009.pdf) (HU Berlin Fachkurs, 2009)
* [Course script on sequence analysis](./systems-biology/hu-berlin-fachkurse/Sequence_Analysis_SS2005.pdf) (HU Berlin Fachkurs, 2005)

### Models of cellular subsystems

* [Course script on signaling pathways](./systems-biology/hu-berlin-fachkurse/Signaling_Pathways_WS2009.pdf) (HU Berlin Fachkurs, 2009)
* [Course script on gene expression models](./systems-biology/hu-berlin-fachkurse/Gene_Expression_WS2004.pdf) (HU Berlin Fachkurs, 2004)
* [Lecture script "Gene input functions"](./systems-biology/wolfram_liebermeister/Lecture_Modelling_of_cell_processes_2009/course_script/script_5_gene_input_functions.pdf) (W. Liebermeister, 2009) 
* [Course script on calcium dynamics](./systems-biology/hu-berlin-fachkurse/Calcium_Dynamics_WS2004.pdf) (HU Berlin Fachkurs, 2004)
* [Course script on cell cycle models](./systems-biology/hu-berlin-fachkurse/Cell_Cycle_WS2009.pdf) (HU Berlin Fachkurs, 2009)
* [Course script on circadian rhythms](./systems-biology/hu-berlin-fachkurse/Circadian_Rhythms_WS2007.pdf) (HU Berlin Fachkurs, 2007)
* [Course script on neuronal dynamics](./systems-biology/hu-berlin-fachkurse/Neuronal_Dynamics_WS2007.pdf) (HU Berlin Fachkurs, 2007)
* [Course script on DNA repair](./systems-biology/hu-berlin-fachkurse/DNA_Repair_SS2006.pdf) (HU Berlin Fachkurs, 2006)
* [Course script on chemotaxis](./systems-biology/hu-berlin-fachkurse/Chemotaxis_WS2007.pdf) (HU Berlin Fachkurs, 2007)

### Dynamics of biological processes

* [Course script on pattern formation](./systems-biology/hu-berlin-fachkurse/Pattern_Formation_SS2007.pdf) (HU Berlin Fachkurs, 2007)
* [Course script on population kinetics](./systems-biology/hu-berlin-fachkurse/Population_Kinetics_SS2006.pdf) (HU Berlin Fachkurs, 2006)
* [Course script on selection equations](./systems-biology/hu-berlin-fachkurse/Selection_Equations_SS2009.pdf) (HU Berlin Fachkurs, 2009)
